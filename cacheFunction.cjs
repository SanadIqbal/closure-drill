function cacheFunction(cb) {
  if(arguments.length >=1 && typeof cb === 'function'){
    const cache = {};
    return function(...args) {
      const key = args;
      if (cache[key]) {
        return cache[key];
      } else {
        const result = cb(...args);
        cache[key] = result;
        return result;
      }
    };
  }
  else{
    throw new Error('Error found');
  }
}

module.exports = cacheFunction;
