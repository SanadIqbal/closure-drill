function counterFactory(){
    let value = 0;
    function increment(){
        value+=1;
        return value;
    }
    function decrement(){
        value-=1;
        return value;
    }
    return {increment, decrement};
}

module.exports = counterFactory;