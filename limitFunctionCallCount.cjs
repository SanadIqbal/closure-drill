function limitFunctionCallCount(callback, count) {
  if(typeof callback === 'function' && typeof count === 'number' && arguments.length ===2){
    let callValue = 0;
    return function (...args) {
      if (callValue < count) {
        callValue++;
        return callback(...args);
      }
      else {
        return null;
      }
    };
  }
  else{
    throw new Error('Error found !!!');
  }
}

module.exports = limitFunctionCallCount;


